# Stage 1 -- build the .jar using gradle
#FROM gradle:4.8-jdk8-alpine AS builder
FROM gradle:4.10.3-jdk8-alpine AS builder
COPY --chown=gradle:gradle . /home/gradle/app
WORKDIR /home/gradle/app
#RUN gradle shadowJar --no-daemon
#RUN gradle gs-rest-service --no-daemon
#RUN gradle bootRun --no-daemon
RUN gradle build

# Stage 2 -- copy the .jar from the builder and run it
FROM openjdk:8-jre-slim
EXPOSE 8080
WORKDIR /app
COPY --from=builder /home/gradle/app/build/libs/ .
#COPY --from=builder /home/gradle/app/config.json .
ENTRYPOINT java -jar *.jar